let btn = document.getElementById('pressy');
browser.runtime.onMessage.addListener(message => {
    for (let e of message.images) {
        let site = e.replace("\"", "").replace("\"", "");
        browser.downloads.download({url: site});
    }
    for (let e of message.redditVideos) {
        downloadRedditVideo(e);
    }
});

function downloadRedditVideo(link) {
    let formats = ['720', '480', '360', '240', '144', 'audio'];
    for (let format of formats) {
        let site = link + 'DASH_' + format + '.mp4';
        // alert(site);
        let siteList = site.split('/');
        let filename = siteList[siteList.length-2] + '_' + siteList[siteList.length-1]; 
        browser.downloads.download({url: site, filename: filename});
    }
}

btn.addEventListener('click', () => {
    /* Sending a message from this script to the content script:
    https://developer.mozilla.org/en-US/docs/Mozilla/Add-ons/
    WebExtensions/Modify_a_web_page#Messaging 
    first code example.*/
    let querying = browser.tabs.query({active: true, currentWindow: true});
    querying.then(tabs => browser.tabs.sendMessage(tabs[0].id, "msg", null));
})