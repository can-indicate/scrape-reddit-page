let urlStart = `(?:www|https|http):/{1,2}`;
let urlSite = `(?:imgur\\.com|i\\.imgur\\.com|i\\.redd\\.it|` + 
              `upload\\.wikimedia\\.org(/[a-zA-Z0-9_\-]*)*|` + 
              `cdn[a-z0-9][0-9]?\\.artstation.com(/[a-zA-Z0-9_\-]*)*)/{1,2}`;
let urlFile = `[^"/]*\\.(?:jpg|png|gif|jpeg|JPG|PNG|GIF|JPEG)(\\?[0-9]+)?`;
let  imageRegExp = new RegExp(`"${urlStart}${urlSite}${urlFile}"`, "g");
// regex for reddit videos: https://v\.redd\.it/[a-zA-Z0-9^/]+/
let redditVideoRegExp = new RegExp(`https://v\.redd\.it/[a-zA-Z0-9^/]+/`, "g");
// regex for gfycat videos: https://gfycat\.com/[a-zA-Z]+

/* 
Question to extract all matches for regex in javascript
https://stackoverflow.com/questions/6323417
(Question by gatlin https://stackoverflow.com/users/794937/gatlin,
 edited by Zach Saucier https://stackoverflow.com/users/2065702/zach-saucier)

Problem with getting the javascript regex g modifier to work:
https://stackoverflow.com/questions/47060553/javascript-regexp-g-modifier-not-working
(Question by ManJoey https://stackoverflow.com/users/3575943/manjoey)
Answer at https://stackoverflow.com/a/47060657 by llama
(https://stackoverflow.com/users/8643852/llama)
*/
let docString = document.body.innerHTML.toString();
let imageLinks = docString.match(imageRegExp);
let redditVideoLinks = docString.match(redditVideoRegExp);


browser.runtime.onMessage.addListener(buttonPressed);

function buttonPressed(req, send, response) {
    console.log(redditVideoLinks);
    let imageLinksSet = new Set(imageLinks);
    let imageLinksList = [];
    for (let e of imageLinksSet) {
        imageLinksList.push(e);
    }
    browser.runtime.sendMessage(message={images: imageLinksList, 
                                         redditVideos: redditVideoLinks});
}
